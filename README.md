Encodes and Decodes [JSON Web Tokens](https://jwt.io/).

JWTs are used when authenticating with remote servers.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
