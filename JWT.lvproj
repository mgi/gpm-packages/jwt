﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="GPM Packages" Type="Folder">
			<Property Name="GPM" Type="Bool">true</Property>
			<Item Name="@mgi" Type="Folder">
				<Item Name="base64" Type="Folder">
					<Item Name="Base64" Type="Folder">
						<Item Name="Base64.lvlib" Type="Library" URL="../gpm_packages/@mgi/base64/Base64/Base64.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/base64/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/base64/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/base64/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/base64/README.md"/>
				</Item>
				<Item Name="hash" Type="Folder">
					<Item Name="Hash" Type="Folder">
						<Item Name="Hash.lvlib" Type="Library" URL="../gpm_packages/@mgi/hash/Hash/Hash.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/hash/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/hash/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/hash/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/hash/README.md"/>
				</Item>
				<Item Name="json" Type="Folder">
					<Item Name="JSON" Type="Folder">
						<Item Name="JSON.lvlib" Type="Library" URL="../gpm_packages/@mgi/json/JSON/JSON.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@mgi/json/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@mgi/json/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@mgi/json/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@mgi/json/README.md"/>
				</Item>
			</Item>
		</Item>
		<Item Name="JWT.lvclass" Type="LVClass" URL="../JWT/JWT.lvclass"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
